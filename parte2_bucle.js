let vector = [1, 'dos', true, 5, 'cinco', false];

// a
console.log('Imprimir usando "for":');
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

// b
console.log('Imprimir usando "forEach":');
vector.forEach((valor) => {
  console.log(valor);
});

// c
console.log('Imprimir usando "map":');
vector.map((valor) => {
  console.log(valor);
});

// d
console.log('Imprimir usando "while":');
let index = 0;
while (index < vector.length) {
  console.log(vector[index]);
  index++;
}

// e
console.log('Imprimir usando "for..of":');
for (let valor of vector) {
  console.log(valor);
}