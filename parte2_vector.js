let vector = [1, 'dos', true, 5, 'cinco', false];

// a.
console.log('Vector:', vector);

// b. 
console.log('Primer elemento:', vector[0]);
console.log('Último elemento:', vector[vector.length - 1]);

// c.
vector[2] = 'tres';
console.log('Vector después de modificar el tercer elemento:', vector);

// d. 
console.log('Longitud del vector:', vector.length);

// e. 
vector.push(6);
console.log('Vector después de agregar un elemento al final:', vector);

// f.
const ultimoElemento = vector.pop();
console.log('Elemento eliminado del final:', ultimoElemento);
console.log('Vector después de eliminar el último elemento:', vector);

// g.
vector.splice(Math.floor(vector.length / 2), 0, 'nuevo');
console.log('Vector después de agregar un elemento en la mitad:', vector);

// h.
const primerElemento = vector.shift();
console.log('Elemento eliminado del inicio:', primerElemento);
console.log('Vector después de eliminar el primer elemento:', vector);

// i.
vector.unshift(primerElemento);
console.log('Vector después de agregar el elemento nuevamente al inicio:', vector);